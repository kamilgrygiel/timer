import java.util.Scanner;
import java.util.function.DoubleBinaryOperator;

public class Main {
    private long start;
    private long stop;
    private double summary;

    public Main(){};
    public void start(){
        this.start = System.currentTimeMillis();
    }

    public double stop(){
        this.stop = System.currentTimeMillis();
        this.summary = this.stop - this.start;
        return this.summary/1000;
    }

    public static void main(String[] args) {
    }
}
